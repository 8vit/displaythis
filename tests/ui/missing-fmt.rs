use displaythis::Display;

#[derive(Display, Debug)]
pub enum Error {
    #[display("...")]
    A(usize),
    B(usize),
}

fn main() {}
